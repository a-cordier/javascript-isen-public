const data = {
  quotes: [
    {
      author: `Linux Torvalds`,
      content: `“Talk is cheap. Show me the code."`,
    },
    {
      author: `Harold Abelson`,
      content: `“Programs must be written for people to read, and only incidentally for machines to execute.” `,
    },
    {
      author: `John Woods`,
      content: `“Always code as if the guy who ends up maintaining your code will be a violent psychopath who knows where you live” `,
    },
    {
      author: `Jeff Atwood`,
      content: `“There are two hard things in computer science: cache invalidation, naming things, and off-by-one errors.” `,
    },
    {
      author: `Edward V. Berard`,
      content: `“Walking on water and developing software from a specification are easy if both are frozen.” `,
    },
    {
      author: `Keith Bostic`,
      content: `“Perl – The only language that looks the same before and after RSA encryption.” `
    },
    {
      author: `Muhammad Waseem`,
      content: `“Give a man a program, frustrate him for a day. Teach a man to program, frustrate him for a lifetime.” `
    }
  ],
  twitterBaseUrl: `https://twitter.com/?status=`
}

const app = document.querySelector('#app')

for (var i = data.quotes.length; i > 0; --i) {
  var button = document.createElement('button')
  button.innerHTML = `quote ${i}`
  button.classList.add('button', 'is-primary')
  app.appendChild(button)
  button.addEventListener('click', function(event) {
    console.log(this, event)
    onQuoteChange(i)
  })
}

function onQuoteChange(quoteIndex) {
  var quote = data.quotes[quoteIndex]
  app.querySelector('p.title').innerHTML = quote.content
  app.querySelector('p.subtitle').innerHTML = quote.content
  app.querySelector('a').href = `${data.twitterBaseUrl}${quote.content} ${quote.author}`
}
