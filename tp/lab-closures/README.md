# [LAB] Javascript: DOM, Closures, Scopes

## Prérequis

Un serveur de développement est déclaré dans les dépendances npm du projet.

Pour utiliser ce serveur, vous devez disposer de node et installer les dépendances du projet avec la commande `npm i` (ou `yarn` si vous préférez yarn)

Pour lancer les serveur lancez la commande `npm start` ou `yarn start` à la racine du TP

## STEP 1

Prenez connaissance du code fourni dans le fichier index.js
 
 * Quelles sont vos observations et en quoi peut on le critiquer ?
 * Testez le code dans votre navigateur
 * Que remarque t-on et pourquoi ?

Dupliquez le code puis commentez le reliquat.

## STEP 2

Corrigez le code sans modifier les assignation de variables

 * Utilisez pour se faire une closure
 
Commentez le code et retournez au code précédent

 ## STEP 3
 
Vous pouvez rendre ce code fonctionnel en modifiant une seule et unique assignation de variable

 * Laquelle et pourquoi ? 
 
 ## STEP 4
 
Corrigez le code en mettant un oeuvre à minima les bonnes pratiques suivantes

 * pas de mot clef `var` (utilisez const ou let)
 * pas de `for ... in` (utilisez `for ... of`, `forEach`, `map` ou `reduce` (?!))
 * ajoutez vos éléments au DOM en ayant recours à l'objet DocumentFragment
